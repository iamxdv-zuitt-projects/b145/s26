// Perform aggregation on the fruits database to return computed results
// Practice documentation reading by successfully implementing aggregation syntax.

/* ------------------------------------------------------------------------*/

// 1. Create an activity.js file on where to write and save the solution for the activity
// 2. Use the count operator to count the total number of fruits on sale.


// 3. Use the count operator to count the total number of fruits with stock more than 20.
    // Answer: 
    db.fruits.aggregate(
        [   

            {$match: {"stock": {$gt : 20}} },
            {$count: "stock"}

            
        ]
    );

// 4. Use the average operator to get the average price of fruits onSale per supplier.
        // Answer
        db.fruits.aggregate(
            [
                {$match: {"onSale": true} },
                {$group: 
                    {
                        _id: null,
                        Average: {$avg: "$price"}
                    } 
                }
                
            ]
        );
// 5. Use the max operator to get the highest price of a fruit per supplier.
db.fruits.aggregate(
    [
        {$unwind: "$origin"},
        {
            $group: 
                {
                _id: "$supplierId", Max: {$max: "$price"}
                } 
        }
    ]
  );

// 6. Use the min operator to get the lowest price of a fruit per supplier
db.fruits.aggregate(
    [
        {$unwind: "$origin"},
        {
            $group: 
                {
                _id: "$supplierId", Min: {$min: "$price"}
                } 
        }
    ]
  );
// 7. Create git repository named s26
// 8. Initialize a local git repository, add the remote link and push to git with the commit message of "Add activity code".
// 9. Add the link in Boodle
/* ------------------------------------------------------------------------*/